/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradelampadas;

/**
 *
 * @author Diamond
 */
import org.junit.Assert;
import org.junit.Test;

public class CalculadoraDeLampadasTest {

    @Test
    public void deveUtilizarUmaLampadasDe18wComComodoDeUmMetroQuadrados() {
        Comodo comodo = new Comodo(1, 1);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(18, comodo);
        Assert.assertEquals(1, quantidadeLampadas);
    }

    @Test
    public void deveUtilizarUmLampadasDe36wComComodoDeDoisMetroQuadrados() {
        Comodo comodo = new Comodo(2, 1);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(36, comodo);
        Assert.assertEquals(1, quantidadeLampadas);
    }

    @Test
    public void deveUtilizarTresLampadasDe18wComComodoDeDoisMetroEMeioQuadrados() {
        Comodo comodo = new Comodo(2.5, 1);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(18, comodo);
        Assert.assertEquals(3, quantidadeLampadas);
    }

    @Test
    public void deveUtilizar17LampadasDe18wComComodoDeDezesseeMenosDaMetadeMetrosQuadrados() {
        Comodo comodo = new Comodo(10.5, 1.55);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(18, comodo);
        Assert.assertEquals(17, quantidadeLampadas);

    }
    
     @Test
    public void deveUtilizar7LampadasDe45wComComodoDeDezesseeMenosDaMetadeMetrosQuadrados() {
        Comodo comodo = new Comodo(10.5, 1.55);
        CalculadoraDeLampadas calculadoraDeLampadas = new CalculadoraDeLampadas();
        int quantidadeLampadas = calculadoraDeLampadas.Cacular(45, comodo);
        Assert.assertEquals(7, quantidadeLampadas);

    }

}
